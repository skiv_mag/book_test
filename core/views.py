# Create your views here.
from rest_framework import generics
from rest_framework.response import Response

from core.models import Book
from core.serializers import CreateBookSerializer, GetBookByAuthorserializer, GetBookserializer


class CreateBookView(generics.CreateAPIView):
    serializer_class = CreateBookSerializer

    def create(self, request, *args, **kwargs):
        super().create(request, *args, **kwargs)
        return Response({'message': "Book created!"})


class GetBookView(generics.RetrieveAPIView):
    queryset = Book.objects.all()
    serializer_class = GetBookserializer


class GetBookListByAuthorView(generics.ListAPIView):
    serializer_class = GetBookByAuthorserializer

    def get_queryset(self):
        return Book.objects.filter(**self.kwargs)
