from django.urls import path

from core.views import CreateBookView, GetBookView, GetBookListByAuthorView

urlpatterns = [
    path('book/', CreateBookView.as_view(), name='api-create-book'),
    path('book/<int:pk>/', GetBookView.as_view(), name='api-get-book'),
    path('books/<int:author_id>/', GetBookListByAuthorView.as_view(), name='api-get-author-books'),
]