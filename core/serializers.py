from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from core.models import Author, Book


class CreateBookSerializer(ModelSerializer):
    author = CharField()

    class Meta:
        model = Book
        fields = ('title', 'author', 'category')

    def create(self, validated_data):
        if 'author' in validated_data:
            author, _ = Author.objects.get_or_create(full_name=validated_data.pop('author'))
            validated_data['author'] = author
        return super().create(validated_data)


class BaseAuthorSerializer(ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class GetBookserializer(ModelSerializer):
    author = BaseAuthorSerializer()

    class Meta:
        model = Book
        fields = '__all__'


class GetBookByAuthorserializer(ModelSerializer):

    class Meta:
        model = Book
        fields = ('title', 'category')
