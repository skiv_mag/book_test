from django.db import models


# Create your models here.

# Build an API and data models in Django that allow a user to
#  CREATE: create a book, with the following fields:
# title (e.g. End of the World)
# author (e.g. H.G. Wells)
# category (e.g. Science Fiction)
# unique id
# GET BOOK: given book id, return a book and its info
# GET AUTHOR: given author id, return list of all books written by the author
#
# No need to build a UI, we can test the API endpoints using Postman.
# Provide documentation for how to run the API locally. No need to deploy onto a server.

class Author(models.Model):
    full_name = models.CharField(max_length=256)


class Book(models.Model):
    title = models.CharField(max_length=256, default='Untitled')
    author = models.ForeignKey('Author', null=False, related_name="books", on_delete=models.CASCADE)
    category = models.CharField(max_length=256, null=True, blank=True)
